﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using jasper.Resources;
using System.Diagnostics;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Jasper_GUI;

namespace jasper
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            WinRTSockets cot = new WinRTSockets();
            string b = cot.GETIP();
            cot.ListenOnWinRTSocket(out b);
            Debug.WriteLine(cot.GETIP());
            Connecter con = new Connecter();
            if (con.Connect(b, 4533) == "Success")
                Debug.WriteLine("connected to self");
            /*<Dont need them now>
            string b = "192.168.56.50";
            cot.ListenOnWinRTSocket(out b);
            Connecter c = new Connecter();
            Connecter con = new Connecter();
            <This is for>
            if (con.Connect(b, 4533) == "Success")
                Debug.WriteLine("connected to self");
            char[] d = { '.' };
            string[] t = b.Split(d);
            string ip;
            const int port = 4533;
            List<string> myIP = new List<string>();
            for (int i = 0; i < 256; i++)
            {
                ip = t[0] + '.' + t[1] + '.' + t[2] + '.' + i.ToString();
                Debug.WriteLine(ip.ToString());
                if (con.Connect(ip, port) == "Success")
                {
                    myIP.Add(ip);
                    Debug.WriteLine(ip);
                }
            }
            foreach (string IP in myIP)
            {
                Debug.WriteLine(IP.ToString());
            }
            Debug.WriteLine(c.Connect("192.168.56.50", 4533));
            Debug.WriteLine(c.Connect("192.168.56.56", 4533));
            c.Send("Nkman is sending you a message Plz Receive");
            <Dont need them now> */
            /* ListBox listBox1 = new ListBox();
             listBox1.ItemsSource = myIP;

             listBox1.Width = 140;
             listBox1.SelectionChanged += ListBox_SelectionChanged;
             StackPanel StackPanel = new StackPanel();
             StackPanel.Orientation = System.Windows.Controls.Orientation.Horizontal;
             StackPanel.Children.Add(listBox1);
         }
         private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
         {
             throw new NotImplementedException();
         }
         private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
         {
             Debug.WriteLine(e.ToString());
         }
             * */
        }

        /*private void InitializeComponent()
        {
            throw new NotImplementedException();
        }*/
        public void About(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/About.xaml", UriKind.Relative));
        }
        public void Credits(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Credits.xaml", UriKind.Relative));
        }
        public void Start(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Start.xaml", UriKind.Relative));
        }
        private void Exit(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine(e.ToString());
            throw new ExitException();
        }
        public void StartTool(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/SecondPage.xaml", UriKind.Relative));
        }
    } 
}