﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using System.Diagnostics;

namespace jasper
{
    class WinRTSockets
    {
        private StreamSocketListener _listener = new StreamSocketListener();
        const string PORT = "4533";

        public WinRTSockets()
        {
        }

        async private void Listen()
        {
            Debug.WriteLine("Listening on 4533");
            _listener.ConnectionReceived += listenerConnectionReceived;
            await _listener.BindServiceNameAsync(PORT);
        }

        private void listenerConnectionReceived(StreamSocketListener sender, StreamSocketListenerConnectionReceivedEventArgs args)
        {
            //connected to socket.
            Debug.WriteLine("Someone connected");
            Debug.WriteLine( DateTime.Now.ToString() + "-" + args.Socket.Information.RemoteHostName.DisplayName.ToString() + "\n" );
            DataWriter writer = new DataWriter(args.Socket.OutputStream);
            writer.WriteString("Listening");
            //writer.StoreAsync();
            Read(args.Socket);
        }

        public async void Read(StreamSocket socket)
        {
            //============ CREATE OBJECTS ====================
            //object outValue;
            //socket = (StreamSocket)outValue;
            DataReader Reader = new DataReader(socket.InputStream);
            var charBuffer = new List<int>();
            do
            {
                try
                {
                    uint sizeFieldCount = await Reader.LoadAsync(sizeof(uint));
                    if (sizeFieldCount != sizeof(uint))
                    {
                        break;
                    }
                    //Debug.WriteLine(Reader);
                    string charCode = Reader.ReadString(sizeof(uint));
                    Debug.WriteLine((string)charCode);
                    

                    var chars = new char[charBuffer.Count];
                    for (int i = 0; i < charBuffer.Count; i++)
                    {
                        chars[i] = Convert.ToChar(charBuffer[i]);
                    }
                    var message = new string(chars);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.ToString());
                    break;
                }


            } while (true);


            //=========== GET RID OF DATAREADER ===========
            Reader.DetachBuffer();
            Reader.DetachStream();
            Reader.Dispose();
            charBuffer.Clear();


            //===============================================

        }
        public void IPADD(StreamSocketListener sender, StreamSocketListenerConnectionReceivedEventArgs args)
        {
            Debug.WriteLine(args.Socket.Information.RemoteHostName.DisplayName.ToString());
        }

        public void ListenOnWinRTSocket(out string ipAddressListeningOn)
        {
            ipAddressListeningOn = "";
            foreach (var item in Windows.Networking.Connectivity.NetworkInformation.GetHostNames())
            {
                Debug.WriteLine("Running IP is "+item.DisplayName);
                if (item.IPInformation != null)
                {
                    ipAddressListeningOn = item.DisplayName;
                    break;
                }
            }
            if (ipAddressListeningOn != "")
                Listen();
            else
            {
                Debug.WriteLine(DateTime.Now.ToString() + "-" + "Couln't find ip address"  + "\n");
            }


        }

        public string GETIP()
        {
            return Windows.Networking.Connectivity.NetworkInformation.GetHostNames()[0].DisplayName.ToString();
        }
    }
}
