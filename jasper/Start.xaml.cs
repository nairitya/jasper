﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using System.Globalization;
using Microsoft.Phone.Globalization;
using System.Collections.ObjectModel;
using Microsoft.Phone.UserData;
using jasper.Resources;
using System.Diagnostics;
using jasper;

namespace Jasper_GUI
{
    public partial class Start : PhoneApplicationPage
    {
        public Start()
        {
            InitializeComponent();

            List<AddressBook> source = new List<AddressBook>();
            /*
            string p = "127.17.15.15";
            string pl = "Nairitya Khilari";
            
            string phone = "8439591486";
            string country = "India";
            source.Add(new AddressBook(p, pl));
             * */
            string pl = "Nairitya Khilari";

            /*List<string> myIP = new List<string>();*/

            /*<Newly added>*/
            WinRTSockets cot = new WinRTSockets();
            string b = cot.GETIP(); // --> Should have current IP Address.
            char[] d = { '.' };
            string[] t = b.Split(d);
            string FirstPart = t[0] + '.' + t[1] + '.' + t[2];
            string ip;
            const int port = 4533;
            /*<Newly added>*/

            Connecter con = new Connecter();
            /*
            if (con.Connect("192.168.56.1", port) == "Success")
            {
                Debug.WriteLine("NAIRITYA_SUCCESS");
            }
            Debug.WriteLine(con.Connect("172.17.12.176", port));
             * */
            /*<Ip scanning>*/
            for (int i = 0; i < 256; i++)
            {
                ip = FirstPart + '.' + i.ToString();
                /*Debug.WriteLine(ip.ToString());*/
                if (con.Connect(ip, port) == "Success")
                {
                    /*myIP.Add(ip);*/
                    source.Add(new AddressBook(ip, pl));
                    Debug.WriteLine(ip);
                }
            }
            /*</Ip scanning>*/

            /*
            myIP.Add("127.17.15.16");
            myIP.Add("127.17.15.17");
            myIP.Add("127.17.15.18");
            myIP.Add("127.17.15.19");
            myIP.Add("127.17.15.40");
             * 
            foreach (string q in myIP)
            {
                source.Add(new AddressBook(q, pl));
            }
             * */
            List<AlphaKeyGroup<AddressBook>> DataSource = AlphaKeyGroup<AddressBook>.CreateGroups(source,
                System.Threading.Thread.CurrentThread.CurrentUICulture,
                (AddressBook s) => { return s.LastName; }, true);
            AddrBook.ItemsSource = DataSource;

        }
    }

    public class AddressBook
    {
        public string FirstName
        {
            get;
            set;
        }
        public string LastName
        {
            get;
            set;
        }
        /*
        public string Address
        {
            get;
            set;
        }
        public string Phone
        {
            get;
            set;
        }
        
        public AddressBook(string firstname, string lastname, string address, string phone)
        */
        public AddressBook(string firstname, string lastname)
        {
            this.FirstName = firstname;
            this.LastName = lastname;
            /*
            this.Address = address;
            this.Phone = phone;
             * */
        }
    }
}