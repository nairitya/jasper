﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using System.Diagnostics;

namespace jasper
{
    class Response
    {
        public Response()
        {
        }

        static readonly int PORT = 4533;
        TaskCompletionSource<object> completionSource;
        public Task SendUsingManagedSocketsAsync(string strServerIP)
        {
            completionSource = new TaskCompletionSource<object>();  // enabling an asynchronous task completion
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);  // creating a new socket
            var ipAddress = IPAddress.Parse(strServerIP);
            var endpoint = new IPEndPoint(ipAddress, PORT);  // Setting endpoint
            var args = new SocketAsyncEventArgs();  // creating event args
            args.RemoteEndPoint = endpoint;
            args.Completed += SocketConnectCompleted;
            if (!socket.ConnectAsync(args))
                SocketConnectCompleted(args.ConnectSocket, args);
            return completionSource.Task;
        }

        private void SocketConnectCompleted(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError != System.Net.Sockets.SocketError.Success)
            {
                completionSource.SetException(new Exception("Failed with " + e.SocketError)); 
                CleanUp(e);
                return;
            }
            else
            {
                completionSource.SetResult(null);
            }
            switch(e.LastOperation)
            {
                case SocketAsyncOperation.Connect:
                    HandleConnect(e);
                    break;
                case SocketAsyncOperation.Send:
                    HandleSend(e);
                    break;
            }
        }

        private void HandleConnect(SocketAsyncEventArgs e)
        {
            if (e.ConnectSocket != null)
            {
                //Sending data is missing here.
                CleanUp(e);
            }
        }

        private void HandleSend(SocketAsyncEventArgs e)
        {
            Debug.Writeline("Connection successfull " +e.ToString());
            //Processing data retreived here.
            CleanUp(e);
        }

        private void CleanUp(SocketAsyncEventArgs e)
        {
            if (e.ConnectSocket != null)
            {
                e.ConnectSocket.Shutdown(SocketShutdown.Both);
                e.ConnectSocket.Close();
            }
        }
    }
}
