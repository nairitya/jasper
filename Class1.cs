﻿using System;

namespace jasper
{
    public class TCPServer
    {

        public TCPServer()
        {
        }
        public int CreateServer()
        {
            TcpListener tcpListener = null;
            IPAddress ipAddress = Dns.GetHostEntry("localhost").AddressList[0];
            try
            {
                // Set the listener on the local IP address 
                // and specify the port.
                tcpListener = new TcpListener(ipAddress, 8000);
                tcpListener.Start();
                output = "Waiting for a connection...";
                console.writeline("server listening");
            }
            finally
            {
                return 1;
            }
        }
    }
}
